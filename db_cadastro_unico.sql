
-- ** SCHEMA: cacesso *******************
CREATE SCHEMA cacesso;
COMMENT ON SCHEMA cacesso IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "este esquema deve agrupar todas as entidades que dizem respeito a mecanismo de controle de acesso, seja autenticação, seja autorizacao.", "DataDeImplantacao":""}';

CREATE TABLE cacesso.grupo (
	id bigserial NOT NULL UNIQUE,
	grupo character varying(30) NOT NULL,
	situacao character varying(20) NOT NULL,
	
	primary key (id)
);
COMMENT ON TABLE cacesso.grupo IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "entidade que reune os grupos de usuarios dos sistemas da prefeitura. o grupo eh elemento definidor de um conjunto de competencias de acesso para persistir e/ou ler dados definidos pelas entidades de dominio.", "DataDeImplantacao":""}';
COMMENT ON COLUMN cacesso.grupo.situacao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "este atributo deve ser utilizado com a funcao de marcar a instancia de grupo como estando ativa ou nao para uso do sistema, de forma que desabilita ou habilita todos os usuarios associados a esta instancia de grupo.", "DataDeImplantacao":""}';

CREATE TABLE cacesso.http_method (
	id bigserial NOT NULL UNIQUE,
	http_method character varying(10) NOT NULL,
	
	primary key (id)
);
COMMENT ON TABLE cacesso.http_method IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "o controle de acesso aos dados das entidades de dominio do si eh efeito, entre outras coisas, pelo tipo de mensagem http enviada ao servidor do si (no caso o datasource).", "DataDeImplantacao":""}';

 CREATE TABLE cacesso.usuario (
	id bigserial NOT NULL UNIQUE,
	nome character varying(120) NOT NULL,
	cpf character varying(16) NOT NULL,
	login character varying(25) NOT NULL,
	password character varying(55) NOT NULL,
	email character varying(100) NOT NULL,
	situacao boolean NOT NULL default false, 
	
	primary key (id)
);
COMMENT ON TABLE cacesso.usuario IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "entidade que armazena dados de usuarios que deverao realizar processos de autenticacao e autorizacao nos sis da prefeitura.", "DataDeImplantacao":""}';
COMMENT ON COLUMN cacesso.usuario.situacao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "este atributo deve ser utilizado com a funcao de marcar a instancia de usuario como estando ativa ou nao para uso do sistema.", "DataDeImplantacao":""}';

CREATE TABLE cacesso.uri (
	id bigserial NOT NULL UNIQUE,
	uri character varying(600) NOT NULL,
	
	primary key (id)
);
COMMENT ON TABLE cacesso.uri IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "armazenar dados das uris para que junto com dados de usuarios e metodos_https realizar controle de acesso aos dados dos sis da prefeitura.", "DataDeImplantacao":""}';
 
CREATE TABLE cacesso.lotacao (
	id bigserial NOT NULL UNIQUE,
	usuario_id bigint NOT NULL,
	grupo_id bigint NOT NULL,
	situacao integer NOT NULL,
	data date NOT NULL,
	
	primary key (id),
	foreign key (usuario_id) references cacesso.usuario (id) on update cascade on delete cascade,
	foreign key (grupo_id) references cacesso.grupo (id) on update cascade on delete cascade
);
COMMENT ON TABLE cacesso.lotacao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "armazenar dados de lotacao de usuarios em grupos.", "DataDeImplantacao":""}';
COMMENT ON COLUMN cacesso.lotacao.situacao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "este atributo deve ser utilizado com a funcao de marcar a instancia de lotacao ao estado da lotacao do usuario relativo a um grupo para utilizacao do si.", "DataDeImplantacao":""}';

CREATE TABLE cacesso.permissao (
	id bigserial NOT NULL UNIQUE,
	grupo_id bigint NOT NULL,
	uri_id bigint NOT NULL,
	http_method_id bigint NOT NULL,
	permissao character varying(12) NOT NULL,
	
	primary key (id),
	foreign key (grupo_id) references cacesso.grupo (id) on update cascade on delete cascade,
	foreign key (uri_id) references cacesso.uri (id) on update cascade on delete cascade
);
COMMENT ON TABLE cacesso.permissao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "armazenar dados das permissoes relativas a triade: uri, grupo e metodo http. isso auxiliarah a realizar controle de acesso aos dados dos sis da prefeitura.", "DataDeImplantacao":""}';
COMMENT ON COLUMN cacesso.permissao.permissao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "os valores da permissao dizem respeito a possibilidade de usuario do sistema, habilitado a usar, pode ler ou alterar estado dos dados nas entidades de dados do si.", "DataDeImplantacao":""}';


-- ** SCHEMA: pessoal *******************
CREATE SCHEMA pessoal;
COMMENT ON SCHEMA pessoal IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "este esquema deve agrupar todas as entidades que dizem respeito ao esquema de pessoal (que tipifiquem pessoas de natureza fisica ou juridica), seja autenticação, seja autorizacao.", "DataDeImplantacao":""}';

CREATE TABLE pessoal.pessoa (
	id bigserial NOT NULL UNIQUE,
	data_de_nascimento date,
	nome character varying(120) NOT NULL,
	end_logradouro character varying(120) NOT NULL,
	end_numero character varying(20) NOT NULL,
	end_bairro character varying(120) NOT NULL,
	end_cidade character varying(120) NOT NULL,
	end_uf character varying(2) NOT NULL,
	end_cep character varying(12),
	end_complemento character varying(300),
	telefone_fixo character varying(14),
	telefone_movel character varying(14),
	email character varying(200),
	im character varying(200),
	log_user bigint,
	log_date date,
	
	primary key (id)
);
COMMENT ON TABLE pessoal.pessoa IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "armazenar dados de municipes, funcionarios, pessoas fisicas e pessoas juridicas de forma geral para o municipio.", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.pessoa.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.pessoa.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
 
CREATE TABLE pessoal.pessoa_fisica (
	id bigint NOT NULL UNIQUE,
	cor character varying(20),
	foto character varying(520),
	grau_de_instrucao character varying(20),
	rc_data_do_registro date,
	rc_folha_do_livro character varying(10),
	rc_nome_do_cartorio character varying(120),
	rc_nome_do_livro character varying(120),
	rc_numero character varying(20),
	religiao character varying(40),
	sexo boolean,
	tipo_sanguineo character varying(5),
	necessidade_especial character varying(20),
	resp_responsavel character varying(120),
	resp_mae character varying(120),
	resp_pai character varying(120),
	resp_cpf character varying(16),
	
	primary key (id),
	foreign key (id) references pessoal.pessoa (id) on update cascade on delete cascade
);
COMMENT ON TABLE pessoal.pessoa_fisica IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "ver finalidade de pessoal.pessoa. esta entidade eh uma especializacao daquela.", "DataDeImplantacao":""}';

CREATE TABLE pessoal.pessoa_juridica (
	id bigint NOT NULL UNIQUE,
	cnpj character varying(16) NOT NULL UNIQUE,
	inscricao_estadual character varying (36),
	inscricao_municipal character varying (36),
	razao_social character varying(120) NOT NULL,
	status boolean NOT NULL default false,
	
	primary key (id),
	foreign key (id) references pessoal.pessoa (id) on update cascade on delete cascade
);
COMMENT ON TABLE pessoal.pessoa_juridica IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "ver finalidade de pessoal.pessoa. esta entidade eh uma especializacao daquela.", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.pessoa_juridica.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';

CREATE TABLE pessoal.cidadao (
	id bigint NOT NULL UNIQUE,
	cpf character varying(16) NOT NULL UNIQUE,
	estado_civil character varying(20),
	nacionalidade character varying(30),
	naturalidade character varying(30),
	pis_pasep character varying(30),
	cm_categoria character varying(20),
	cm_data_de_emissao date,
	cm_numero character varying(20),
	ct_data_de_emissao date,
	ct_numero character varying(28),
	ct_serie character varying(20),
	rg_data_de_emissao date,
	rg_numero character varying(16),
	rg_orgao_expeditor character varying(20),
	rg_uf character varying(2),
	te_numero character varying(20),
	te_secao character varying(12),
	te_zona character varying(12),
	fp_formacao character varying(20),
	fp_numero_de_ordem_do_conselho character varying(20),
	fp_grau_de_instrucao character varying(12),
	fp_profissao character varying(20),
	fp_conselho character varying(20),
	cnh_numero character varying(20),
	cnh_categoria character varying(26),
	cnh_validade date,
	ss_nis character varying(30) UNIQUE,
	ss_numero_caixa character varying(16),
	ss_numero_registro character varying(20),
	status boolean NOT NULL default false,
	
	primary key (id),
	foreign key (id) references pessoal.pessoa_fisica (id) on update cascade on delete cascade
);
COMMENT ON TABLE pessoal.cidadao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "ver finalidade de pessoal.pessoa_fisica. esta entidade eh uma especializacao daquela.", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.cidadao.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';

CREATE TABLE pessoal.funcionario (
	id bigserial NOT NULL UNIQUE,
	cidadao_id bigint NOT NULL,
	data_de_admissao date,
	data_de_demissao date,
	matricula character varying NOT NULL UNIQUE,
	end_logradouro character varying(120),
	end_numero character varying(10),
	end_bairro character varying(120),
	end_cidade character varying(120),
	end_uf character varying(2),
	end_cep character varying(12),
	end_complemento character varying(300),
	telefone_fixo character varying(14),
	telefone_movel character varying(14),
	email character varying(200),
	im character varying(120),
	tipo_de_contrato character varying,
	validado boolean NOT NULL default false,
	status boolean NOT NULL default false,
	log_user bigint,
	log_date date,
	
	primary key (id),
	foreign key (cidadao_id) references pessoal.cidadao (id) on update cascade on delete cascade
);
COMMENT ON TABLE pessoal.funcionario IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "descrever cidadao pela sua perspectiva de relacao com o agente empregador, no caso, a prefeitura", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.funcionario.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.funcionario.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.funcionario.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

CREATE TABLE pessoal.prefeitura (
	id bigint NOT NULL UNIQUE,
	brasao character varying(520),
	status boolean NOT NULL default false,
	
	primary key (id),
	foreign key (id) references pessoal.pessoa_juridica (id) on update cascade on delete cascade
);
COMMENT ON TABLE pessoal.prefeitura IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "ver finalidade de pessoal.pessoa_juridica. esta entidade eh uma especializacao daquela", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.prefeitura.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';


-- ** SCHEMA: convenios *******************
CREATE SCHEMA convenio;
COMMENT ON SCHEMA convenio IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "este esquema deve agrupar todas as entidades que dizem respeito ao esquema de convenios celebrados pela prefeitura com instituicoes da sociedade.", "DataDeImplantacao":""}';

CREATE TABLE convenio.edital (
	id bigserial NOT NULL UNIQUE,
	identificador_de_edital character varying(36) NOT NULL UNIQUE,
	edital_doc_dir character varying(520),
	data_de_publicacao date,
	data_de_realizacao date,
	objeto character varying(500),
	status boolean NOT NULL default false,
	log_user bigint,
	log_date date,
	
	primary key (id)
);
COMMENT ON TABLE convenio.edital IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "descreve as varias instancias de convenios celebrados pelo municipio.", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.edital.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.edital.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.edital.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

CREATE TABLE convenio.modalidade (
	id bigserial NOT NULL UNIQUE,
	modalidade character varying(36) NOT NULL UNIQUE,
	status boolean NOT NULL default false,
	log_user bigint,
	log_date date,
	
	primary key (id)
);
COMMENT ON TABLE convenio.modalidade IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "descreve as modalidades de convenios instituidas pela legislacao vigente.", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.modalidade.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.modalidade.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.modalidade.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
 
CREATE TABLE convenio.convenio (
	id bigserial NOT NULL UNIQUE,
	edital_id bigint NOT NULL,
	modalidade_id bigint NOT NULL,
	identificador_de_convenio character varying(36) NOT NULL UNIQUE,
	data_de_inicio date NOT NULL,
	data_de_termino date,
	status boolean NOT NULL default false,
	log_user bigint,
	log_date date,
	
	primary key (id),
	foreign key (edital_id) references convenio.edital (id) on update cascade on delete cascade,
	foreign key (modalidade_id) references convenio.modalidade (id) on update cascade on delete cascade
);
COMMENT ON TABLE convenio.convenio IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "descreve as modalidades de convenios instituidas pela legislacao vigente.", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.convenio.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.convenio.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.convenio.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

CREATE TABLE convenio.conveniado (
	id bigserial NOT NULL UNIQUE,
	prefeitura_id bigint NOT NULL,
	pessoa_juridica_id bigint NOT NULL,
	status boolean NOT NULL default false,
	log_user bigint,
	log_date date,
	
	primary key (id),
	foreign key (prefeitura_id) references pessoal.prefeitura (id) on update cascade on delete cascade,
	foreign key (pessoa_juridica_id) references pessoal.pessoa_juridica (id) on update cascade on delete cascade
);
COMMENT ON TABLE convenio.conveniado IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "entidade cuja finalidade eh capturar os dados das relacoes de convenios celebrados pelo municipio", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.conveniado.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.conveniado.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.conveniado.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

-- ** SCHEMA: contrato *******************
CREATE SCHEMA contrato;
